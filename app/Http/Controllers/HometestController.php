<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HometestController extends Controller
{
    public function hometest()
    {
        $isim = "Atilla";
        $soyisim = "OKTAY";
        $isimler = ['Atilla','Özlem','Burak','Ahmet','Zeynep'];
        $kullanicilar = [
            ['id'=>1, 'kullanici_adi'=>'Ati'],
            ['id'=>2, 'kullanici_adi'=>'Pisi'],
            ['id'=>3, 'kullanici_adi'=>'Kanka'],
            ['id'=>4, 'kullanici_adi'=>'Torpil Abi'],
            ['id'=>5, 'kullanici_adi'=>'Bisikletli']
        ];
        //return view('homepage', ['isim'=>'Ati', 'soyisim'=>'OKTAY']); view içerisine değişken göndermek
        //return view('homepage')->with(['isim'=>$isim, 'soyisim'=>$soyisim]);
        return view ('hometest', compact('isim','soyisim','isimler','kullanicilar')); //compact fonksiyonu değişken değerlerinden dizi oluşturmayı sağlar.
        //return view('hometest');
    }

    public function test()
    {
        return view ('test');
    }

    public function test2()
    {
        return view ('test2');
    }

    public function index($slug_test)
    {
        return view('test2');
    }
}
