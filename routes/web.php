<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('anasayfa');
});*/

Route::get('/', 'HomepageController@index')->name('homepage');
Route::get('/content', 'HomepageController@content')->name('content');

// Aşağı kısım test alanı :) //

Route::get('/hometest','HometestController@hometest')->name('hometest');
Route::get('/test','HometestController@test')->name('test');
Route::get('/test2','HometestController@test2')->name('test2');
Route::get('/test2/{slug_test}','HometestController@index')->name('slugtest');
//Route::view('/test','test');
//Route::view('/test2','test2');
