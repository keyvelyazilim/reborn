<!-- Start footer area -->
<footer id="footer" class="footer-wrapper footer-1">
    <!-- Start footer top area -->
    <div class="footer-top-wrap ptb-70 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 hidden-sm">
                    <div class="zm-widget pr-40">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Bisikletli Kadın İnisiyatifi Hakkında</h2>
                        <div class="zm-widget-content">
                            <p>Bisikletli Kadın İnisiyatifi, gündelik hayatta bisiklet süren veya sürmek isteyen kadınların oluşturduğu bir topluluk olup, sadece kadınlara özel bir facebook grubu üzerinden koordinasyonunu sağlar.</p>
                            <p>Grup sadece bisiklet süren kadınlara açık olmayıp, "ah keşke bisiklet sürmeyi öğrensem", "trafiğe çıkmaya korkuyorum" diyen kadınları da kucaklar.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-6 col-lg-2">
                    <div class="zm-widget">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Sosyal Medya</h2>
                        <div class="zm-widget-content">
                            <div class="zm-social-media zm-social-1">
                                <ul>
                                    <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i>Facebook grubuna üye ol</a></li>
                                    <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i>Twitter'da bizi takip et</a></li>
                                    <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i>Youtube kanalına abone ol</a></li>
                                    <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i>Instagram'da bizi beğen</a></li>
                                    <li><a href="mailto:bki@bisikletlikadin.com?Subject=Merhaba" target="_top"><i class="fa fa-at"></i>Bize mail ile ulaş</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-6 col-lg-3">
                    <div class="zm-widget pr-50 pl-20">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Popüler Kategoriler</h2>
                        <div class="zm-widget-content">
                            <div class="zm-category-widget zm-category-1">
                                <ul>
                                    <li><a href="#">Bisikletli Yaşam<span>22</span></a></li>
                                    <li><a href="#">Buluşma<span>18</span></a></li>
                                    <li><a href="#">Etkinlik<span>09</span></a></li>
                                    <li><a href="#">Basın<span>35</span></a></li>
                                    <li><a href="#">Sizden Gelenler<span>7</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-6 col-lg-3">
                    <div class="zm-widget">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Bisikletli Kadın E-Bülten</h2>
                        <!-- Start Subscribe From -->
                        <div class="zm-widget-content">
                            <div class="subscribe-form subscribe-footer">
                                <p>Türkiye'deki 6500'den fazla kadının üye olduğu topluluğumuzdan haberdar olmak için.</p>
                                <form action="#">
                                    <input type="text" placeholder="İsim Soyad">
                                    <input type="email" placeholder="Eposta Adres" required="">
                                    <input type="submit" value="E-Bültene Kaydol">
                                </form>
                            </div>
                        </div>
                        <!-- End Subscribe From -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer top area -->
    <div class="footer-buttom bg-black ptb-15">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="zm-copyright">
                        <p class="uppercase">&copy; {{ $yil }} Bisikletli Kadın İnisiyatifi. Tüm hakları saklıdır.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-right hidden-xs">
                    <nav class="footer-menu zm-secondary-menu text-right">
                        <ul>
                            <li><a href="#">Ana Sayfa</a></li>
                            <li><a href="#">Hakkımızda</a></li>
                            <li><a href="#">Serbest Kürsü</a></li>
                            <li><a href="#">Basında Biz</a></li>
                            <li><a href="#">İletişim</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End footer area -->