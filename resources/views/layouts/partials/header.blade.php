<!-- Start of header area -->
<header  class="header-area header-wrapper bg-white clearfix">
    <!-- Start Sidebar Menu -->
    <div class="sidebar-menu">
        <div class="sidebar-menu-inner"></div>
        <span class="fa fa-remove"></span>
    </div>
    <!-- End Sidebar Menu -->
    <div class="header-top-bar bg-dark ptb-10">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7  hidden-xs">
                    <div class="header-top-left">
                        <nav class="header-top-menu zm-secondary-menu">
                            <ul>
                                <li><a href="#">Anasayfa</a></li>
                                <li><a href="#">Hakkımızda</a></li>
                                <li><a href="#">Serbest Kürsü </a></li>
                                <li><a href="#">Basında Biz</a></li>
                                <li><a href="#">İletişim</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                    <div class="header-top-right clierfix text-right">
                        <div class="header-social-bookmark topbar-sblock">
                            <ul>
                                <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="mailto:bki@bisikletlikadin.com?Subject=Merhaba" target="_top"><i class="fa fa-at"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="zmaga-calendar topbar-sblock">
                            <span class="calendar uppercase">22 Ocak 2019</span>
                        </div>
                        <div class="user-accoint topbar-sblock">
                            <span class="login-btn uppercase">Giriş</span>
                            <div class="login-form-wrap bg-white">
                                <form class="zm-signin-form text-left">
                                    <input type="text" class="zm-form-control username" placeholder="Email">
                                    <input type="password" class="zm-form-control password" placeholder="Şifre">
                                    <input type="checkbox" value=" Remember Me" class="remember"> &nbsp;Beni Hatırla<br>
                                    <div class="zm-submit-box clearfix  mt-20">
                                        <input type="submit" value="Login">
                                        <a href="registration.html">Kayıt</a>
                                    </div>
                                    <a href="#" class="zm-forget">Şifremi Unuttum!</a>
                                    <div class="zm-login-social-box">
                                        <a href="#" class="social-btn bg-facebook block"><span class="btn_text"><i class="fa fa-facebook"></i>Facebook ile giriş yap!</span></a>
                                        <a href="#" class="social-btn bg-twitter block"><span class="btn_text"><i class="fa fa-twitter"></i>Twitter ile giriş yap!</span></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-5 col-xs-12 header-mdh">
                    <div class="global-table">
                        <div class="global-row">
                            <div class="global-cell">
                                <div class="logo">
                                    <a href="index.html">
                                        <img src="/images/logo/logo.png" height="100" alt="bki logo">
                                    </a>
                                    <!-- <p class="site-desc">Bisikletli Kadın İnisiyatifi</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-7 col-sm-7 col-xs-12 col-lg-offset-1 header-mdh hidden-xs">
                    <div class="global-table">
                        <div class="global-row">
                            <div class="global-cell">
                                <div class="advertisement text-right">
                                    <a href="#" class="block"><img src="/images/ad/header-btn.png" alt="bki destek"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom-area hidden-sm hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu-wrapper  bg-theme clearfix">
                        <div class="row">
                            <div class="col-md-11">
                                <button class="sidebar-menu-btn">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                                <div class="mainmenu-area">
                                    <nav class="primary-menu uppercase">
                                        <ul class="clearfix">
                                            <li><a href="#">Ana Sayfa</a></li>
                                            <li class="current drop"><a href="#">Biz Kimiz</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Hakkımızda</a></li>
                                                    <li><a href="#">Manifesto</a></li>
                                                    <li><a href="#">Temsilciler</a></li>
                                                </ul>
                                            </li>
                                            <li class="drop"><a href="#">Ne Yaparız</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Bisiklet Eğitimleri</a></li>
                                                    <li><a href="#">Sürüş Etkinlikleri</a></li>
                                                    <li><a href="#">Buluşmalar</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Pedallayan Kadınlar</a></li>
                                            <li><a href="#">Etkinlikler</a></li>
                                            <li class="drop"><a href="#">Bisikletli Yaşam</a>
                                                <ul class="dropdown">
                                                    <li><a href="#">Nasıl Başlarım</a></li>
                                                    <li><a href="#">Bakım-Onarım</a></li>
                                                    <li><a href="#">Bisiklet Festivalleri</a></li>
                                                    <li><a href="#">Bisiklet Grupları</a></li>
                                                    <li><a href="#">Film ve Belgeseller</a></li>
                                                    <li><a href="#">Bisiklet Çeşitleri</a></li>
                                                    <li><a href="#">Bisiklet Seçimi</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Sizden Gelenler</a></li>
                                            <li><a href="#">Basın</a></li>
                                            <li><a href="#">İletişim</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="search-wrap pull-right">
                                    <div class="search-btn"><i class="fa fa-search"></i></div>
                                    <div class="search-form">
                                        <form action="#">
                                            <input type="search" placeholder="Arama">
                                            <button type="submit"><i class='fa fa-search'></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area start -->
    <div class="mobile-menu-area hidden-md hidden-lg">
        <div class="fluid-container">
            <div class="mobile-menu clearfix">
                <div class="search-wrap mobile-search">
                    <div class="mobile-search-btn"><i class="fa fa-search"></i></div>
                    <div class="mobile-search-form">
                        <form action="#">
                            <input type="text" placeholder="Search">
                            <button type="submit"><i class='fa fa-search'></i></button>
                        </form>
                    </div>
                </div>
                <nav id="mobile_dropdown">
                    <ul>
                        <li><a href="#">Ana Sayfa</a>
                        <li class="current drop"><a href="#">Biz Kimiz</a>
                            <ul class="dropdown level2">
                                <li><a href="#">Hakkımızda</a></li>
                                <li><a href="#">Manifesto</a></li>
                                <li><a href="#">Temsilciler</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Ne Yaparız</a>
                            <ul class="dropdown level2">
                                <li><a href="#">Bisiklet Eğitimleri</a></li>
                                <li><a href="#">Sürüş Etkinlikleri</a></li>
                                <li><a href="#">Buluşmalar</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Pedallayan Kadınlar</a></li>
                        <li><a href="#">Etkinlikler</a></li>
                        <li><a href="#">Bisikletli Yaşam</a>
                            <ul class="dropdown level2">
                                <li><a href="#">Nasıl Başlarım</a></li>
                                <li><a href="#">Bakım-Onarım</a></li>
                                <li><a href="#">Bisiklet Festivalleri</a></li>
                                <li><a href="#">Bisiklet Grupları</a></li>
                                <li><a href="#">Film ve Belgeseller</a></li>
                                <li><a href="#">Bisiklet Çeşitleri</a></li>
                                <li><a href="#">Bisiklet Seçimi</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Sizden Gelenler</a></li>
                        <li><a href="#">Basın</a></li>
                        <li><a href="#">İletişim</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area end -->
    <div class="breakingnews-wrapper hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="breakingnews clearfix fix">
                        <div class="bn-title">
                            <h6 class="uppercase">SERBEST KÜRSÜ</h6>
                        </div>
                        <div class="news-wrap">
                            <ul class="bkn clearfix" id="bkn">
                                <li>14.02.2019 Maltepe'den Kadıköy'e saat 11:00'de yola çıkacağım eşlik etmek isteyen arkadaşlar? | @ozlemokt</li>
                                <li>Yarın akşam Açık Radyo'da Trapez Kadro programına bekleriz... | @editor</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End of header area -->