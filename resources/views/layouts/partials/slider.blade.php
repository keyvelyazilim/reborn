<!-- Start trending post area -->
<div class="trending-post-area">
    <div class="container-fluid">
        <div class="row">
            <div class="trend-post-list zm-posts owl-active-1 clearfix">
                <!-- Start single trend post -->
                <div class="col-2">
                    <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="2.5"  data-scrim-bottom="9" data-effict-zoom="3">
                        <div class="zm-post-thumb">
                            <img src="images/post/trend/a/1.jpg" alt="img">
                        </div>
                        <div class="zm-post-dis text-white">
                            <div class="zm-category"><a href="#" class="bg-cat-3 cat-btn">Buluşma</a></div>
                            <h2 class="zm-post-title"><a href="#">Caddebostan sahilde bisiklet bakım onarım eğitimi.</a></h2>
                            <div class="zm-post-meta">
                                <ul>
                                    <li class="s-meta"><a href="#" class="zm-author">Özlem OKTAY</a></li>
                                    <li class="s-meta"><a href="#" class="zm-date">2 Şubat 2018, 10:30</a></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End single trend post -->
                <!-- Start single trend post -->
                <div class="col-2">
                    <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="2.5"  data-scrim-bottom="9" data-effict-zoom="3">
                        <div class="zm-post-thumb">
                            <img src="images/post/trend/a/2.jpg" alt="img">
                        </div>
                        <div class="zm-post-dis text-white">
                            <div class="zm-category"><a href="#" class="bg-cat-2 cat-btn">Etkinlik</a></div>
                            <h2 class="zm-post-title"><a href="#">Yazın ilk turunda aramızda olmaya ne dersiniz? :)</a></h2>
                            <div class="zm-post-meta">
                                <ul>
                                    <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                    <li class="s-meta"><a href="#" class="zm-date">15 Haziran 2019 11:00</a></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End single trend post -->
                <!-- Start single trend post -->
                <div class="col-2">
                    <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="2.5"  data-scrim-bottom="9" data-effict-zoom="3">
                        <div class="zm-post-thumb">
                            <img src="images/post/trend/a/10.jpg" alt="img">
                        </div>
                        <div class="zm-post-dis text-white">
                            <div class="zm-category"><a href="#" class="bg-cat-5 cat-btn">Basın</a></div>
                            <h2 class="zm-post-title"><a href="haber.html">Bisikletli Kadınlar Facebook sahnesinde. <span style="color:yellow">&lsaquo;TIKLA&rsaquo;</span></a></h2>
                            <div class="zm-post-meta">
                                <ul>
                                    <li class="s-meta"><a href="#" class="zm-author">Özlem OKTAY</a></li>
                                    <li class="s-meta"><a href="#" class="zm-date">19 Şubat 2018</a></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End single trend post -->
                <!-- Start single trend post -->
                <div class="col-2">
                    <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="2.5"  data-scrim-bottom="9" data-effict-zoom="3">
                        <div class="zm-post-thumb">
                            <img src="images/post/trend/a/4.jpg" alt="img">
                        </div>
                        <div class="zm-post-dis text-white">
                            <div class="zm-category"><a href="#" class="bg-cat-4 cat-btn">Bisikletli Yaşam</a></div>
                            <h2 class="zm-post-title"><a href="#">Kadınlar için bisiklet kılavuzu yayınladı.</a></h2>
                            <div class="zm-post-meta">
                                <ul>
                                    <li class="s-meta"><a href="#" class="zm-author">Seçil YAKAN</a></li>
                                    <li class="s-meta"><a href="#" class="zm-date">15 Haziran 2017</a></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End single trend post -->
                <!-- Start single trend post -->
                <div class="col-2">
                    <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="2.5"  data-scrim-bottom="9" data-effict-zoom="3">
                        <div class="zm-post-thumb">
                            <img src="images/post/trend/a/11.jpg" alt="img">
                        </div>
                        <div class="zm-post-dis text-white">
                            <div class="zm-category"><a href="#" class="bg-cat-1 cat-btn">Sizden Gelenler</a></div>
                            <h2 class="zm-post-title"><a href="#">Kapadokya'da Bisiklet Sürüş Deneyimi</a></h2>
                            <div class="zm-post-meta">
                                <ul>
                                    <li class="s-meta"><a href="#" class="zm-author">Gülay ÇAMURSOY</a></li>
                                    <li class="s-meta"><a href="#" class="zm-date">28 Eylül 2018</a></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End single trend post -->
                <!-- Start single trend post -->
                <div class="col-2">
                    <article class="zm-trending-post zm-lay-a zm-single-post" data-dark-overlay="2.5"  data-scrim-bottom="9" data-effict-zoom="3" >
                        <div class="zm-post-thumb">
                            <img src="images/post/trend/a/12.jpg" alt="img">
                        </div>
                        <div class="zm-post-dis text-white">
                            <div class="zm-category"><a href="#" class="bg-cat-2 cat-btn">Etkinlik</a></div>
                            <h2 class="zm-post-title"><a href="#">Trafikte bisikletli kadın atölyesi.</a></h2>
                            <div class="zm-post-meta">
                                <ul>
                                    <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                    <li class="s-meta"><a href="#" class="zm-date">1 Kasım 2018</a></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <!-- End single trend post -->
            </div>
        </div>
    </div>
</div>
<!-- End trending post area -->