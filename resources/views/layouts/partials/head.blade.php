<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">

<!-- All css files are included here. -->
<!-- Bootstrap fremwork main css -->
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<!-- This core.css file contents all plugings css file. -->
<link rel="stylesheet" href="{{ asset('css/core.css') }}">
<!-- Theme shortcodes/elements style -->
<link rel="stylesheet" href="{{ asset('css/shortcode/shortcodes.css') }}">
<!-- Theme main style -->
<link rel="stylesheet" href="{{ asset('style.css') }}">
<!-- Responsive css -->
<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
<!-- User style -->
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

<!-- Style customizer (Remove these lines please) -->
<link rel="stylesheet" href="{{ asset('css/style-customizer.css') }}">
{{--<link href="#" data-style="styles" rel="stylesheet">--}}

<!-- Modernizr JS -->

<script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>