<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    {{--<title>@yield('title', config('app.name'))</title>--}}
    <title>{{config('app.name')}} - @yield('title')</title>
    @include('layouts.partials.head')
    @yield('head')
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!--  THEME PRELOADER AREA -->
<div id="preloader-wrapper">
    <div class="preloader-wave-effect"></div>
</div>
<!-- THEME PRELOADER AREA END -->

<!-- Body main wrapper start -->
<div class="wrapper">
    @include('layouts.partials.header')
    @yield('content')
    @include('layouts.partials.footer',['yil'=>2019])
</div>
<!-- Body main wrapper end -->

@include('layouts.partials.footerjs')
@yield('footer')
</body>
</html>