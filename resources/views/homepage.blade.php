@extends('layouts.master')
@section('title','Ana Sayfa')
@section('content')
@include('layouts.partials.slider')

<!-- Start page content -->
<section id="page-content" class="page-wrapper">
    <!-- Start Popular News [layout A+D]  -->
    <div class="zm-section bg-white ptb-70">
        <div class="container">
            <div class="row mb-40">
                <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                    <div class="section-title">
                        <h2 class="h6 header-color inline-block uppercase">Popüler Haberler</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12 col-lg-6">
                    <div class="zm-posts">
                        <article class="zm-post-lay-a">
                            <div class="zm-post-thumb">
                                <a href="#"><img src="images/post/a/1.jpg" alt="img"></a>
                            </div>
                            <div class="zm-post-dis">
                                <div class="zm-post-header">
                                    <div class="zm-category"><a href="#" class="bg-cat-4 cat-btn">Bisikletli Yaşam</a></div>
                                    <h2 class="zm-post-title h2"><a href="#">Don Kişot kolektifi bisiklet ve kent çalıştayı yapıldı.</a></h2>
                                    <div class="zm-post-meta">
                                        <ul>
                                            <li class="s-meta"><a href="#" class="zm-author">Seçil ZOR</a></li>
                                            <li class="s-meta"><a href="#" class="zm-date">03 Ocak 2019</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="zm-post-content">
                                    <p>Kolektifin bu yıl ikinci kez organize ettiği bisiklet ve kent çalıştayı, 12-13 Ocak 2019 tarihlerinde Saat 13:00-19:00'da İstanbul Mimarlar Odası Büyükkent Şubesi Karaköy'de yapıldı.</p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12 col-lg-6">
                    <div class="zm-posts">
                        <!-- Start single post layout D -->
                        <article class="zm-post-lay-d clearfix">
                            <div class="zm-post-thumb f-left">
                                <a href="#"><img src="images/post/d/1.jpg" alt="img"></a>
                            </div>
                            <div class="zm-post-dis f-right">
                                <div class="zm-post-header">
                                    <div class="zm-category"><a href="#" class="bg-cat-4 cat-btn">Bisikletli Yaşam</a></div>
                                    <h2 class="zm-post-title"><a href="#">Emel SALI'den bisikletli anneler için ipuçları...</a></h2>
                                    <div class="zm-post-meta">
                                        <ul>
                                            <li class="s-meta"><a href="#" class="zm-author">Emel SALI</a></li>
                                            <li class="s-meta"><a href="#" class="zm-date">18 Aralık 2019</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <!-- Start single post layout D -->
                        <!-- End single post layout D -->
                        <article class="zm-post-lay-d clearfix">
                            <div class="zm-post-thumb f-left">
                                <a href="#"><img src="images/post/d/2.jpg" alt="img"></a>
                            </div>
                            <div class="zm-post-dis f-right">
                                <div class="zm-post-header">
                                    <div class="zm-category"><a href="#" class="bg-cat-3 cat-btn">Buluşma</a></div>
                                    <h2 class="zm-post-title "><a href="#">Bisikletli Kadınlar Edirne'de buluşuyor.</a></h2>
                                    <div class="zm-post-meta">
                                        <ul>
                                            <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            <li class="s-meta"><a href="#" class="zm-date">29 Haziran 2018</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <!-- Start single post layout D -->
                        <!-- End single post layout D -->
                        <article class="zm-post-lay-d clearfix">
                            <div class="zm-post-thumb f-left">
                                <a href="#"><img src="images/post/d/3.jpg" alt="img"></a>
                            </div>
                            <div class="zm-post-dis f-right">
                                <div class="zm-post-header">
                                    <div class="zm-category"><a href="#" class="bg-cat-4 cat-btn">Bisikletli Yaşam</a></div>
                                    <h2 class="zm-post-title "><a href="#">Bisikletli Kadınlar belgeseli Cannes Film Festivalinde prömierini yapıyor.</a></h2>
                                    <div class="zm-post-meta">
                                        <ul>
                                            <li class="s-meta"><a href="#" class="zm-author">Özlem OKTAY</a></li>
                                            <li class="s-meta"><a href="#" class="zm-date">1 Nisan 2019</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <!-- Start single post layout D -->
                        <!-- End single post layout D -->
                        <article class="zm-post-lay-d clearfix">
                            <div class="zm-post-thumb f-left">
                                <a href="#"><img src="images/post/d/4.jpg" alt="img"></a>
                            </div>
                            <div class="zm-post-dis f-right">
                                <div class="zm-post-header">
                                    <div class="zm-category"><a href="#" class="bg-cat-2 cat-btn">Etkinlik</a></div>
                                    <h2 class="zm-post-title "><a href="#">Hale SARGIN'ın söyleşisi yarın akşam Bisikletli Mekan'da. Hepiniz davetlisiniz :)</a></h2>
                                    <div class="zm-post-meta">
                                        <ul>
                                            <li class="s-meta"><a href="#" class="zm-author">Zeynep ARABOĞLU</a></li>
                                            <li class="s-meta"><a href="#" class="zm-date">20 Ocak 2019</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <!-- End single post layout D -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Popular News [layout A+D]  -->

    <!-- Start world news [layout A1+E+A]  -->
    <div class="zm-section bg-gray ptb-70">
        <div class="container">
            <div class="row">
                <!-- Start left side -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="row mb-40">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="section-title">
                                <h2 class="h6 header-color inline-block uppercase">Bisikletli Yaşam</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="zm-posts clearfix">
                            <!-- Start single post layout B -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                    <div class="zm-post-thumb">
                                        <a href="#" data-dark-overlay="2.5"  data-scrim-bottom="9"><img src="images/post/trend/b/1.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis text-white">
                                        <h2 class="zm-post-title h3"><a href="#">Bakım ve Onarım </a></h2>
                                        <div class="zm-post-meta">
                                            <ul>
                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- End single post layout B -->
                            <!-- Start single post layout B -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                    <div class="zm-post-thumb">
                                        <a href="#" data-dark-overlay="2.5"  data-scrim-bottom="9"><img src="images/post/trend/b/2.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis text-white">
                                        <h2 class="zm-post-title h3"><a href="#">Bisiklet Festivalleri </a></h2>
                                        <div class="zm-post-meta">
                                            <ul>
                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- End single post layout B -->
                            <!-- Start single post layout B -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                    <div class="zm-post-thumb">
                                        <a href="#" data-dark-overlay="2.5"  data-scrim-bottom="9"><img src="images/post/trend/b/3.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis text-white">
                                        <h2 class="zm-post-title h3"><a href="#">Bisiklet Grupları </a></h2>
                                        <div class="zm-post-meta">
                                            <ul>
                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- End single post layout B -->
                            <!-- Start single post layout B -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                    <div class="zm-post-thumb">
                                        <a href="#" data-dark-overlay="2.5"  data-scrim-bottom="9"><img src="images/post/trend/b/4.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis text-white">
                                        <h2 class="zm-post-title h3"><a href="#">Film ve Belgeseller </a></h2>
                                        <div class="zm-post-meta">
                                            <ul>
                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- End single post layout B -->
                            <!-- Start single post layout B -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                    <div class="zm-post-thumb">
                                        <a href="#" data-dark-overlay="2.5"  data-scrim-bottom="9"><img src="images/post/trend/b/5.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis text-white">
                                        <h2 class="zm-post-title h3"><a href="#">Bisiklet Çeşitleri</a></h2>
                                        <div class="zm-post-meta">
                                            <ul>
                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- End single post layout B -->
                            <!-- Start single post layout B -->
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                    <div class="zm-post-thumb">
                                        <a href="#" data-dark-overlay="2.5"  data-scrim-bottom="9"><img src="images/post/trend/b/6.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis text-white">
                                        <h2 class="zm-post-title h3"><a href="#">Bisiklet Seçimi </a></h2>
                                        <div class="zm-post-meta">
                                            <ul>
                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <!-- End single post layout B -->
                        </div>
                    </div>
                    <!-- Start Advertisement -->
                    <div class="advertisement">
                        <div class="row mt-40">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                <a href="#"><img src="images/ad/2.jpg" alt="bki destek"></a>
                            </div>
                        </div>
                    </div>
                    <!-- End Advertisement -->
                </div>
                <!-- End left side -->

                <!-- Start Righr side -->
                <div class="col-xs-12 col-sm-12 hidden-sm col-md-4 col-lg-4">
                    <!-- Start post layout E -->
                    <aside class="zm-post-lay-e-area">
                        <div class="row mb-40">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="section-title">
                                    <h2 class="h6 header-color inline-block uppercase">Pedallayan Kadınlar</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="zm-posts">
                                    <!-- Start single post layout E -->
                                    <article class="zm-post-lay-e zm-single-post clearfix">
                                        <div class="zm-post-thumb f-left">
                                            <a href="#"><img src="images/post/e/1.jpg" alt="img"></a>
                                        </div>
                                        <div class="zm-post-dis f-right">
                                            <div class="zm-post-header">
                                                <h2 class="zm-post-title"><a href="#">Aygül ORHAN, üç yıldır işine bisikletle gidiyor.</a></h2>
                                                <div class="zm-post-meta">
                                                    <ul>
                                                        <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                        <li class="s-meta"><a href="#" class="zm-date">25 Aralık 2018</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Start single post layout E -->
                                    <!-- Start single post layout E -->
                                    <article class="zm-post-lay-e zm-single-post clearfix">
                                        <div class="zm-post-thumb f-left">
                                            <a href="#"><img src="images/post/e/2.jpg" alt="img"></a>
                                        </div>
                                        <div class="zm-post-dis f-right">
                                            <div class="zm-post-header">
                                                <h2 class="zm-post-title"><a href="#">İrem ÇAĞIL kızıyla birlikte bisikletle Kuzey Avrupa'yı dolaştı.</a></h2>
                                                <div class="zm-post-meta">
                                                    <ul>
                                                        <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                        <li class="s-meta"><a href="#" class="zm-date">25 Mayıs 2017</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Start single post layout E -->
                                    <!-- Start single post layout E -->
                                    <article class="zm-post-lay-e zm-single-post clearfix">
                                        <div class="zm-post-thumb f-left">
                                            <a href="#"><img src="images/post/e/3.jpg" alt="img"></a>
                                        </div>
                                        <div class="zm-post-dis f-right">
                                            <div class="zm-post-header">
                                                <h2 class="zm-post-title"><a href="#">Şeyma ŞAHİN, bankadaki işini bırakıp, bisiklet dükkanı açtı.</a></h2>
                                                <div class="zm-post-meta">
                                                    <ul>
                                                        <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                        <li class="s-meta"><a href="#" class="zm-date">14 Şubat 2017</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Start single post layout E -->
                                    <!-- Start single post layout E -->
                                    <article class="zm-post-lay-e zm-single-post hidden-md clearfix">
                                        <div class="zm-post-thumb f-left">
                                            <a href="#"><img src="images/post/e/4.jpg" alt="img"></a>
                                        </div>
                                        <div class="zm-post-dis f-right">
                                            <div class="zm-post-header">
                                                <h2 class="zm-post-title"><a href="#">Aslıhan SEVEN, Polenezköy'deki nikah törenini bisikletiyle gitti.</a></h2>
                                                <div class="zm-post-meta">
                                                    <ul>
                                                        <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                        <li class="s-meta"><a href="#" class="zm-date">19 Eylül 2016</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- Start single post layout E -->
                                </div>
                            </div>
                        </div>
                    </aside>
                    <!-- Start post layout E -->
                    <!-- Start Day News -->
                    <aside class="zm-post-lay-a-area  hidden-sm">
                        <div class="row mb-40 mt-70">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="section-title">
                                    <h2 class="h6 header-color inline-block uppercase">Trapez Kadro</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="zm-posts">
                                    <article class="zm-post-lay-a sidebar">
                                        <div class="zm-post-thumb">
                                            <a href="#"><img src="images/post/b/1.jpg" alt="img"></a>
                                        </div>
                                        <div class="zm-post-dis">
                                            <div class="zm-post-header">
                                                <h2 class="zm-post-title h2"><a href="#">Bu akşamın konukları Atilla OKTAY ve Ahmet TORPİL.</a></h2>
                                                <div class="zm-post-meta">
                                                    <ul>
                                                        <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                        <li class="s-meta"><a href="#" class="zm-date">29 Ocak 2019 19:00</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <!-- Start Day News -->
                </div>
                <!-- End Right side -->
            </div>
        </div>
    </div>
    <!-- End world news [layout A1+E+A]  -->
    <!-- Start Video Post [View layout A]  -->
    <div class="video-post-area ptb-70 bg-dark">
        <div class="container">
            <div class="row mb-40">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="section-title">
                        <h2 class="h6 header-color inline-block uppercase">Videolar</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="zm-video-post-list zm-posts owl-active-3 navigator-1 clearfix">
                    <!-- Start single video post [video layout A] -->
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="zm-video-post zm-video-lay-a zm-single-post">
                            <div class="zm-video-thumb"  data-dark-overlay="2.5" >
                                <img src="http://img.youtube.com/vi/j48hqA32Jn4/maxresdefault.jpg" width="420" height="270" alt="video">
                                <a class="video-activetor" href="https://www.youtube.com/watch?v=j48hqA32Jn4">
                                    <i class="fa fa-play-circle-o"></i>
                                </a>
                            </div>
                            <div class="zm-video-info text-white">
                                <h2 class="zm-post-title"><a href="blog-single-video.html">İstanbul bisiklete binmek için uygun bir şehir mi?</a></h2>
                                <div class="zm-post-meta">
                                    <ul>
                                        <li class="s-meta zm-post-time">0:53 min</li>
                                        <li class="s-meta"><a href="#" class="zm-author">Journo</a></li>
                                        <li class="s-meta"><a href="#" class="zm-date">13 Eylül 2016</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End single video post [video layout A] -->
                    <!-- Start single video post [video layout A] -->
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="zm-video-post zm-video-lay-a zm-single-post">
                            <div class="zm-video-thumb" data-dark-overlay="2.5" >
                                <img src="http://img.youtube.com/vi/TmbSG8-TL7o/maxresdefault.jpg" width="420" height="270" alt="video">
                                <a class="video-activetor" href="https://www.youtube.com/watch?v=TmbSG8-TL7o">
                                    <i class="fa fa-play-circle-o"></i>
                                </a>
                            </div>
                            <div class="zm-video-info  text-white">
                                <h2 class="zm-post-title"><a href="blog-single-video.html">Bisikletli Kadın İnisiyatifi - Bilmeyenlere Bisiklet Eğitimi. </a></h2>
                                <div class="zm-post-meta">
                                    <ul>
                                        <li class="s-meta zm-post-time">0:51 min</li>
                                        <li class="s-meta"><a href="#" class="zm-author">Bisikletli Kadın İnisiyatifi</a></li>
                                        <li class="s-meta"><a href="#" class="zm-date">25 Mart 2016</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End single video post [video layout A] -->
                    <!-- Start single video post [video layout A] -->
                    <div class="col-xs-12 hiddne-sm col-md-4 col-lg-4">
                        <div class="zm-video-post zm-video-lay-a zm-single-post">
                            <div class="zm-video-thumb" data-dark-overlay="2.5" >
                                <img src="http://img.youtube.com/vi/13Qia8dJsjs/maxresdefault.jpg" width="420" height="270" alt="video">
                                <a class="video-activetor" href="https://www.youtube.com/watch?v=13Qia8dJsjs">
                                    <i class="fa fa-play-circle-o"></i>
                                </a>
                            </div>
                            <div class="zm-video-info  text-white">
                                <h2 class="zm-post-title"><a href="blog-single-video.html">Bisikletli Kadın İnisiyatifi - Haftasonu Sürüş Etkinliği.</a></h2>
                                <div class="zm-post-meta">
                                    <ul>
                                        <li class="s-meta zm-post-time">1:43 min</li>
                                        <li class="s-meta"><a href="#" class="zm-author">Bisikletli Kadın İnisiyatifi</a></li>
                                        <li class="s-meta"><a href="#" class="zm-date">10 Nisan 2016</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End single video post [video layout A] -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Video Post [View layout A]  -->
    <div class="zm-section bg-white pt-70 pb-40">
        <div class="container">
            <div class="row">
                <!-- Start left side -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 columns">
                    <div class="row mb-40">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="section-title">
                                <h2 class="h6 header-color inline-block uppercase">En Çok Okunanlar</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="zm-posts">
                                <!-- Start single post layout C -->
                                <article class="zm-post-lay-c zm-single-post clearfix">
                                    <div class="zm-post-thumb f-left">
                                        <a href="#"><img src="images/post/c/1.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis f-right">
                                        <div class="zm-post-header">
                                            <div class="zm-category"><a href="#" class="bg-cat-4 cat-btn">Bisikletli Yaşam</a></div>
                                            <h2 class="zm-post-title"><a href="#">Kadınlar için bisiklet kılavuzu yayınladı.</a></h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta"><a href="#" class="zm-author">Seçil YAKAN</a></li>
                                                    <li class="s-meta"><a href="#" class="zm-date">15 Haziran 2017</a></li>
                                                </ul>
                                            </div>
                                            <div class="zm-post-content">
                                                <p>Kadınlar için gündelik hayatta bisiklet binmeyi kolaylaştırıcı bilgi ve deneyimlerin yer aldığı kılavuz yayınladı.</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!-- Start single post layout C -->
                                <!-- Start single post layout C -->
                                <article class="zm-post-lay-c zm-single-post clearfix">
                                    <div class="zm-post-thumb f-left">
                                        <a href="#"><img src="images/post/c/2.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis f-right">
                                        <div class="zm-post-header">
                                            <div class="zm-category"><a href="#" class="bg-cat-1 cat-btn">Sizden Gelenler</a></div>
                                            <h2 class="zm-post-title"><a href="#">Kapadokya'da Bisiklet Sürüş Deneyimi</a></h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta"><a href="#" class="zm-author">Gülay ÇAMURSOY</a></li>
                                                    <li class="s-meta"><a href="#" class="zm-date">28 Eylül 2018</a></li>
                                                </ul>
                                            </div>
                                            <div class="zm-post-content">
                                                <p>Kapadokya'nın eşşiz volkanik arazisinde birçok bisiklet parkuru var. Bisiklet sürmek için gerçekten enfes bir yer.</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!-- Start single post layout C -->
                                <!-- Start single post layout C -->
                                <article class="zm-post-lay-c zm-single-post clearfix">
                                    <div class="zm-post-thumb f-left">
                                        <a href="#"><img src="images/post/c/3.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis f-right">
                                        <div class="zm-post-header">
                                            <div class="zm-category"><a href="#" class="bg-cat-3 cat-btn">Buluşma</a></div>
                                            <h2 class="zm-post-title"><a href="#">Caddebostan sahilde bisiklet bakım onarım eğitimi.</a></h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta"><a href="#" class="zm-author">Özlem OKTAY</a></li>
                                                    <li class="s-meta"><a href="#" class="zm-date">2 Şubat 2018, 10:30</a></li>
                                                </ul>
                                            </div>
                                            <div class="zm-post-content">
                                                <p>Kim demiş kadınlar bisiklet tamir edemez diye. Bu kadınlar hem trafikte bisiklet sürüyor hem de tamir ediyor.</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!-- Start single post layout C -->
                                <!-- Start Advertisement -->
                                <!--                                         <div class="advertisement">
                                                                            <div class="row mtb-40">
                                                                                <div class="col-md-12 text-center">
                                                                                    <a href="#"><img src="images/ad/2.jpg" alt="bki destek"></a>
                                                                                </div>
                                                                            </div>
                                                                        </div> -->
                                <!-- End Advertisement -->
                                <!-- Start single post layout C -->
                                <article class="zm-post-lay-c zm-single-post clearfix">
                                    <div class="zm-post-thumb f-left">
                                        <a href="#"><img src="images/post/c/4.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis f-right">
                                        <div class="zm-post-header">
                                            <div class="zm-category"><a href="#" class="bg-cat-5 cat-btn">Basın</a></div>
                                            <h2 class="zm-post-title"><a href="haber.html">Bisikletli Kadınlar Facebook sahnesinde. <span style="color:red">&lsaquo;TIKLA&rsaquo;</span></a></h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta"><a href="#" class="zm-author">Özlem OKTAY</a></li>
                                                    <li class="s-meta"><a href="#" class="zm-date">19 Şubat 2018</a></li>
                                                </ul>
                                            </div>
                                            <div class="zm-post-content">
                                                <p>Facebook “Communities Summit Europe (FCS)“ etkinliği ile topluluk liderlerini Londra’da bir araya getirdi. Avrupa’dan 150 grubun katılığı etkinlikte Türkiye’den de temsilciler yer aldı. Türkiye katılımcılardan öne çıkan topluluk ise Bisikletli Kadın İnisiyatifi oldu.</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!-- Start single post layout C -->
                                <!-- Start single post layout C -->
                                <article class="zm-post-lay-c zm-single-post clearfix">
                                    <div class="zm-post-thumb f-left">
                                        <a href="#"><img src="images/post/c/5.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis f-right">
                                        <div class="zm-post-header">
                                            <div class="zm-category"><a href="#" class="bg-cat-4 cat-btn">Etkinlik</a></div>
                                            <h2 class="zm-post-title"><a href="#">Hale SARGIN'ın söyleşisi yarın akşam Bisikletli Mekan'da. Hepiniz davetlisiniz :)</a></h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta"><a href="#" class="zm-author">Zeynep ARABOĞLU</a></li>
                                                    <li class="s-meta"><a href="#" class="zm-date">20 Ocak 2019</a></li>
                                                </ul>
                                            </div>
                                            <div class="zm-post-content">
                                                <p>"İşim Gücüm Gezmek" blogunun sahibi gezgin kadın bisikletli Hale SARGIN yarın "Bisikletli Mekan"da bisikletli kadınlarla buluşacak.</p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <!-- Start single post layout C -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End left side -->
                <!-- Start Right sidebar -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 sidebar-warp columns">
                    <div class="row">
                        <!-- Start Subscribe From -->
                        <div class="col-md-6 col-sm-12 col-xs-12 col-lg-12 xs-mt-40 sm-mt-50 md-mt-60">
                            <aside class="subscribe-form bg-dark text-center sidebar">
                                <h3 class="uppercase zm-post-title">Bisikletli Kadın E-Bülten</h3>
                                <p>Türkiye'deki 6500'den fazla kadının üye olduğu topluluğumuzdan haberdar olmak için</p>
                                <form action="#">
                                    <input type="text" placeholder="İsim Soyad">
                                    <input type="email" placeholder="Eposta Adresi" required="">
                                    <input type="submit" value="E-Bültene Kaydol">
                                </form>
                            </aside>
                        </div>
                        <!-- End Subscribe From -->
                        <aside class="zm-tagcloud-list col-xs-12 col-sm-6 col-md-6 col-lg-12 mt-60">
                            <div class="row mb-40">
                                <div class="col-md-12">
                                    <div class="section-title">
                                        <h2 class="h6 header-color inline-block uppercase">Etiketler</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="zm-tagcloud">
                                        <a href="#">Bisiklet Eğitimleri</a>
                                        <a href="#">Sürüş Etkinlikleri</a>
                                        <a href="#">Buluşmalar</a>
                                        <a href="#">Pedallayan Kadınlar</a>
                                        <a href="#">Bisikletli Yaşam</a>
                                        <a href="#">Etkinlik</a>
                                        <a href="#">Bisiklet Festivallari</a>
                                        <a href="#">Basın</a>
                                        <a href="#">Film ve Belgeseller</a>
                                        <a href="#">Nasıl Başlarım</a>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <!-- Start post layout E -->
                        <aside class="zm-post-lay-e-area col-xs-12 col-sm-6 col-md-6 col-lg-12 mt-60 hidden-md">
                            <div class="row mb-40">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="section-title">
                                        <h2 class="h6 header-color inline-block uppercase">Kadın ve Kent</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="zm-posts">
                                        <!-- Start single post layout E -->
                                        <article class="zm-post-lay-e zm-single-post clearfix">
                                            <div class="zm-post-thumb f-left">
                                                <a href="#"><img src="images/post/kadin-ve-kent/1.jpg" alt="img"></a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title"><a href="#">Kadın Dostu Kent Değil, Kent Dostu Kadınlar Var</a></h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">Çiçek TAHAOĞLU</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">26 Eylül 2014</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Start single post layout E -->
                                        <!-- Start single post layout E -->
                                        <article class="zm-post-lay-e zm-single-post clearfix">
                                            <div class="zm-post-thumb f-left">
                                                <a href="#"><img src="images/post/kadin-ve-kent/2.jpg" alt="img"></a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title"><a href="#">Bisiklet, Kadınların Toplumda Görünür Olması İçin Bir Fırsattır</a></h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">Pınar PİNZUTİ</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">26 Haziran 2017</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Start single post layout E -->
                                        <!-- Start single post layout E -->
                                        <article class="zm-post-lay-e zm-single-post hidden-md clearfix">
                                            <div class="zm-post-thumb f-left">
                                                <a href="#"><img src="images/post/kadin-ve-kent/3.jpg" alt="img"></a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title"><a href="#">'Süslü kadınlar', egzoz kokusu yerine parfüm kokusu için pe...</a></h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">Sema GÜR</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">23 Eylül 2018</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Start single post layout E -->
                                        <!-- Start single post layout E -->
                                        <article class="zm-post-lay-e zm-single-post hidden-md clearfix hidden-sm">
                                            <div class="zm-post-thumb f-left">
                                                <a href="#"><img src="images/post/kadin-ve-kent/4.jpg" alt="img"></a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title"><a href="#">Toplumsal cinsiyetin kent ve mekanla ilişkisi ne?</a></h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">KAOS GL</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">09 Mart 2017</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Start single post layout E -->
                                        <!-- Start single post layout E -->
                                        <article class="zm-post-lay-e zm-single-post hidden-md clearfix hidden-sm">
                                            <div class="zm-post-thumb f-left">
                                                <a href="#"><img src="images/post/kadin-ve-kent/5.jpg" alt="img"></a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title"><a href="#">Alternatif Ulaşım Aracı Olarak Bisikleti Düşünmek</a></h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">Nevra TAŞLIDAN</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">06 Aralık 2010</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Start single post layout E -->
                                        <!-- Start single post layout E -->
                                        <article class="zm-post-lay-e zm-single-post hidden-md clearfix hidden-sm">
                                            <div class="zm-post-thumb f-left">
                                                <a href="#"><img src="images/post/kadin-ve-kent/6.jpg" alt="img"></a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <h2 class="zm-post-title"><a href="#">Ulaşımda ‘Pembe’ Ayrımcılığına Karşı Çıkmamız İçin 5 Neden</a></h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta"><a href="#" class="zm-author">Editör</a></li>
                                                            <li class="s-meta"><a href="#" class="zm-date">19 Ekim 2017</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                        <!-- Start single post layout E -->
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <!-- Start post layout E -->
                    </div>
                </div>
                <!-- End Right sidebar -->
            </div>
            <!-- Start pagination area -->
            <!--                     <div class="row hidden-xs">
                                    <div class="zm-pagination-wrap mt-70">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <nav class="zm-pagination ptb-40 text-center">
                                                        <ul class="page-numbers">
                                                            <li><a class="prev page-numbers" href="#">Previous</a></li>
                                                            <li><span class="page-numbers current">1</span></li>
                                                            <li><a class="page-numbers" href="#">2</a></li>
                                                            <li><a class="page-numbers" href="#">3</a></li>
                                                            <li><a class="page-numbers" href="#">4</a></li>
                                                            <li><a class="page-numbers" href="#">5</a></li>
                                                            <li><span class="page-numbers zm-pgi-dots">...</span></li>
                                                            <li><a class="page-numbers" href="#">15</a></li>
                                                            <li><a class="next page-numbers" href="#">Next</a></li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
            <!-- End pagination area -->
            <!-- Start Advertisement -->
            <div class="advertisement">
                <div class="row mt-40">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <a href="#"><img src="images/ad/2.jpg" alt="bki destek"></a>
                    </div>
                </div>
            </div>
            <!-- End Advertisement -->
        </div>
    </div>
</section>
<!-- End page content -->
@endsection