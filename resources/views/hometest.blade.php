<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ReBorn</title>
    </head>
    <body>
    @php
    $yas = 31;
    @endphp
        <h3>Merhaba, {{$isim .' '. $soyisim}}, {{$yas}} yaşındasın. </h3>
    <hr>
    @if ($isim == 'Atilla')
        Hoşgeldin Atilla
    @elseif ($isim = 'Özlem')
        Hoşgeldin Özlem
    @else
        Hoşgeldin
    @endif
    <hr>
    @switch($isim)
        @case('Atilla')
        Hoşgeldin Atilla
        @break

        @case('Özlem')
        Hoşgeldin Özlem
        @break

        @default
        Hoşgeldin
    @endswitch
    <hr> {{-- for döngüsü --}}
    @for($i=0; $i<=10; $i++)
        Döngü Değeri {{ $i }}
    @endfor
    <hr> {{--  while döngüsü --}}
    @php $i=0; @endphp
    @while($i<=10)
        Döngü Değeri {{ $i }}
        @php $i++; @endphp
    @endwhile
    <hr> {{--  foreach döngüsü --}}
    @foreach($isimler as $isim)
        {{-- {{$isim}}, //son ismin sonunda da virgül işareti olduğu için aşağıda farklı bir kodlama yapılıyor--}}
        {{ $isim. ($isim !== end($isimler) ? ',' : '') }}
        {{-- isim değeri döngünün son değerinden farklıysa virgül boşlk eğer aynıysa hiçbir şey yazma --}}
    @endforeach
    <hr>
    @foreach($kullanicilar as $kullanici)
        @continue($kullanici['id'] == 1) {{-- 1 numarayı atla --}}
        <li> {{ $kullanici['id'] . '-' . $kullanici['kullanici_adi'] }}</li>
        @break($kullanici['id']==4) {{-- 4 numaraya gelince döngüyü durdur --}}
    @endforeach
    <hr>
    @php $html = "<b>Test</b>"; @endphp
    {{ $html  }} Değişkenden Html değer basmak için { ve ! kullanımı <br>
    {!! $html !!}
    <hr>
    </body>
</html>
