@extends('layouts.master')
@section('title','İçerik')
@section('content')

    <!-- Start page content -->
    <div id="page-content" class="page-wrapper">
        <div class="zm-section single-post-wrap bg-white ptb-30 xs-pt-30">
            <div class="container">
                <div class="row">
                    <!-- Start left side -->
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 columns">
                        <div class="row mb-40">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="section-title">
                                    <h2 class="h6 header-color inline-block uppercase">Basından Haberler</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Start single post image formate-->
                            <div class="col-md-12">
                                <article class="zm-post-lay-single">
                                    <div class="zm-post-thumb">
                                        <a href="blog-single-image.html"><img src="/images/post/single/1.jpg" alt="img"></a>
                                    </div>
                                    <div class="zm-post-dis">
                                        <div class="zm-post-header">
                                            <h2 class="zm-post-title h2"><a href="blog-single-image.html">Bisikletli Kadınlar Facebook Sahnesinde</a></h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta"><a href="#" class="zm-author">Özlem OKTAY</a></li>
                                                    <li class="s-meta"><a href="#" class="zm-date">19 Şubat 2018</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="zm-post-content">
                                            <p>Facebook, 2017 yılının başından beri bir yandan sahte haberler ve hesaplar, sansür ve kutuplaşma ile mücadele ederken bir yandan da sosyal medya bağımlılığına sebebiyet verdiği gerekçesiyle gelen eleştiriler ile uğraşıyor. Bu sorunlarla mücadele etmek için sosyal medya kullanımının olumlu yönlerini ortaya çıkarıyor ve organik oluşan anlamlı grupların topluma en iyi katkılardan biri olarak görüyor.</p>

                                            <p>Buradan hareketle Facebook, ilkini geçtiğimiz sene Chicago’da düzenlediği “Topluluk Liderleri Zirvesi“ne, bu yıl Londra ile devam etti. 8-9 Şubat tarihinde gerçekleşen etkinlik, Avrupa’daki Facebook topluluk  liderlerini bir araya getirdi. Facebook Communities Summit Europe (FCS) etkinliğinde Londra’da buluşturdu.</p>

                                            <blockquote>Facebook “Communities Summit Europe (FCS)“ etkinliği ile topluluk liderlerini Londra’da bir araya getirdi. Avrupa’dan 150 grubun katılığı etkinlikte Türkiye’den de temsilciler yer aldı. Türkiye katılımcılardan öne çıkan topluluk ise Bisikletli Kadın İnisiyatifi oldu.</blockquote>

                                            <p>Buradan hareketle Facebook, ilkini geçtiğimiz sene Chicago’da düzenlediği “Topluluk Liderleri Zirvesi“ne, bu yıl Londra ile devam etti. 8-9 Şubat tarihinde gerçekleşen etkinlik, Avrupa’daki Facebook topluluk  liderlerini bir araya getirdi. Facebook Communities Summit Europe (FCS) etkinliğinde Londra’da buluşturdu.</p>

                                            <p>Avrupa’dan 150 grubun katıldığı etkinliğe Türkiye’den Bisikletli Kadın İnisiyatifi, Kalpler Erisin, Interrail Türkiye, İhtiyaç Haritası, Mide Lobisi, Saçım Saçın Olsun ve Araştıran Anneler katıldı.</p>

                                            <p>Bisikletli Kadın İnisiyatifi, zirveye katılan Avrupa’daki en önemli topluluklar arasında seçilerek Facebook Ürün Ortakları Başkan Yardımcısı Ime Archibong tarafından sahneye davet edildi. Grubun kurucu üyeleri Seçil Öznur Yakan ve Zeynep Araboğlu sahnede, Bisikletli Kadın İnisiyatifi’nin oluşum hikayesini, sosyal medyada ve gerçek yaşamda grup üyeleriyle iletişimi ve yaptıkları etkinlikleri paylaştı.</p>

                                            <p>Bisikletli Kadın İnisiyatifi Kurucu Üyesi Seçil Öznur Yakan zirveyle ilgili değerlendirmesinde “Facebook, demokratik bir dünya için gruplar aracılığı ile insanlar arası iletişimin daha güçlü olması gerektiğine inanıyor. Facebook mühendisler ve ürün geliştiricileri ile atölyeler yaptık. Grup içinde ne gibi sorunlar yaşadığımızı dinlediler ve grup iletişimi nasıl geliştireceğimize dair çalışmalar yaptık. Bisikletli Kadın İnisiyatifi online değil daha çok offline olarak kullanılıyor. Kadınlarla birlikte sürüş yapıyoruz, bisiklet binmeyi öğretiyoruz. Grubu daha uzaktaki üyelerimizle haberleşmek için kullanıyoruz.” dedi.</p>

                                            <p>Zeynep Araboğlu “Anlamlı gruplara yönelik destek çalışmalarını son derece doğru buluyorum. Gündelik hayatı güçlendirici bir rol almak istiyor. Burada birçok grupla tanıştık. Kendi kültürünü oluşturan oldukça değişik gruplar vardı. Bu işten para kazanan veya gönüllü hareket eden, çevreyi temizleyen, sosyalleşmeye yönelik etkinlikler yapan, anne ve baba olmaya yönelik destek veren ve daha birçok alanda faaliyet gösteren gruplar vardı.  Bisikletli Kadın İnisiyatifi olarak etkinlikte ayrı bir ilgi gördük. Örnek grup olarak Keynote’larda sahne aldık. Etkinlik boyunca yapılan çalışmalardan şunu gördük, gruplar sosyal medya üzerinden kendi kültürünü oluşturmayı ve iletişimi ne ölçüde başarıyor, bunu gerçek sosyal hayata ne kadar taşıyabiliyor, bu Facebook için başarı göstergesi olarak kabul ediliyor.“</p>

                                            <p>Facebook zirvede anlamlı grupları güçlendirmek için “Topluluk Liderliği Programı” ilan etti. Bu küresel girişimin amacı; insanların gruplarla daha fazla etkileşimini sağlamak, dünyadaki topluluk liderlerini güçlendirmek için eğitim, destek ve fon sağlamak. Facebook Topluluk Liderliği Programı kapsamında seçilecek 5 gruba 1.000.000 dolara kadar ödül verecek. Burs programı kapsamında seçilecek 100 lidere topluluk inisiyatifi için kullanılmak üzere 50.000 dolar fon sağlayacak. Topluluk Liderlik Çemberleri ile yerel topluluk liderlerini birbirine bağlayarak iş birliği yapmaları için bir araya getirecek ve eğitimler verecek.</p>

                                        </div>
                                        <div class="entry-meta-small clearfix ptb-40 mtb-40 border-top border-bottom">
                                            <div class="meta-list pull-left">
                                                <span class="post-title">Etiketler</span>
                                                <a href="#">Facebook</a>,<a href="#">BKİ</a>,<a href="#">Seyahat</a>,<a href="#">Community</a>
                                            </div>
                                            <div class="share-social-link pull-right">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                                <a href="#"><i class="fa fa-rss"></i></a>
                                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                            </div>
                                        </div>
                                        <div class="administrator-info clearfix">
                                            <div class="administrator-avatar">
                                                <img alt="administrator" src="/images/post/single/author/1.jpg">
                                            </div>
                                            <div class="administrator-description">
                                                <h4 class="post-title"><a href="#">Özlem OKTAY Hakkında</a></h4>
                                                <p>2015 yılında, Türkiye'de daha çok kadının bisikleti kullanmasına teşvik etmek için oluşturulan Bisikletli Kadın İnisiyatifi'ni kuran yedi kadından biri oldu. Bisikletli Kadın İnisiyatifi ile birçok etkinliğe, eğitime, sürüşe katıldı.
                                                    2017 yılında toplumun aktif bir yaşam sürmesi için projeler geliştiren Aktif Yaşam Derneği'nde Sarı Bisiklet projesinin yöneticilerinden biri oldu. Sarı Bisiklet ile ulusal çapta birçok proje gerçekleştirdi. Yine 2017 yılında Lübnan'da  düzenlenen 20 ülkeden 120 kadının katıldığı Follow The Women - Kadınlar Barış İçin Pedallıyor projesine Türkiye'den katılan 8 kadından biri oldu. 8 gün boyunca Lübnan'da sürüş yaparak, farklı dernek ve mülteci kamplarında buluşmalara katıldı. </p>
                                                <div class="share-social-link">
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                                    <a href="#"><i class="fa fa-rss"></i></a>
                                                    <a href="#"><i class="fa fa-dribbble"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>

                            <!-- Start Comment box  -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="review-area mt-50 ptb-70 border-top">
                                    <div class="post-title mb-40">
                                        <h2 class="h6 inline-block">Bu haber hakkında 3 yorum bulunmakta</h2>
                                    </div>
                                    <div class="review-wrap">
                                        <div class="review-inner">
                                            <!-- Start Single Review -->
                                            <div class="single-review clearfix">
                                                <div class="reviewer-img">
                                                    <img src="/images/post/single/comnt/1.jpg" alt="">
                                                </div>
                                                <div class="reviewer-info">
                                                    <h4 class="reviewer-name"><a href="#">Atilla OKTAY</a></h4>
                                                    <span class="date-time">20 Ocak 2018 10:30</span>
                                                    <p class="reviewer-comment">Merhaba, Facebook Community toplantısına katılmak için nasıl bir başvuru yapmamız gerekiyor?</p>
                                                    <a href="#" class="reply-btn">Yanıtla</a>
                                                </div>
                                            </div>
                                            <!-- End Single Review -->
                                            <!-- Start Single Review -->
                                            <div class="single-review second-comment clearfix">
                                                <div class="reviewer-img">
                                                    <img src="/images/post/single/comnt/2.jpg" alt="">
                                                </div>
                                                <div class="reviewer-info">
                                                    <h4 class="reviewer-name"><a href="#">Özlem OKTAY</a></h4>
                                                    <span class="date-time">20 Ocak 2018 11:00</span>>
                                                    <p class="reviewer-comment">Merhaba, Atilla Bey biz başvuru yapmadık. Facebook, grubumuzun organik büyümesinden etkilendiği için bizi davet etti. Konu ile ilgili olarak mail atarsanız, ilgili arkadaşlara gerekli yönlendirmeyi sağlayabilirim. </p>
                                                    <a href="#" class="reply-btn">Yanıtla</a>
                                                </div>
                                            </div>
                                            <!-- End Single Review -->
                                            <!-- Start Single Review -->
                                            <!-- <div class="single-review third-comment clearfix">
                                                <div class="reviewer-img">
                                                    <img src="/images/post/single/comnt/2.jpg" alt="">
                                                </div>
                                                <div class="reviewer-info">
                                                    <h4 class="reviewer-name"><a href="#">Jhon doe</a></h4>
                                                    <span class="date-time">Auguest 11, 2.16, 12:21pm</span>
                                                    <p class="reviewer-comment">Phasellus aliquam ante metus, vitae molestie turpis rutrum eu. Fusce mi risus, cursus eu elit vitae, viverra vulputate mi. Donec ullamcorper felis nec sapien ultrices feugiat. Morbi sit amet blandit nibh. Vivamus eros felis, tempus vitae dolor quis, euismod dignissim ipsum. </p>
                                                <a href="#" class="reply-btn">Reply</a>
                                                </div>
                                            </div> -->
                                            <!-- End Single Review -->
                                            <!-- Start Single Review -->
                                            <div class="single-review clearfix">
                                                <div class="reviewer-img">
                                                    <img src="/images/post/single/comnt/3.jpg" alt="">
                                                </div>
                                                <div class="reviewer-info">
                                                    <h4 class="reviewer-name"><a href="#">Burak GÜL</a></h4>
                                                    <span class="date-time">22 Ocak 2018 17:15</span>
                                                    <p class="reviewer-comment">Özlem Hanım Merhaba, eşim ile birlikte bisiklete binmek istiyoruz. Ancak beni terk etti. Bu konu hakkında yardımcı olabilir misiniz? Kendisine ulaşamıyorum. Fakat bir bki üyesi olduğunu biliyorum. Şimdiden teşekkürler.</p>
                                                    <a href="#" class="reply-btn">Yanıtla</a>
                                                </div>
                                            </div>
                                            <!-- End Single Review -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Comment box  -->
                            <!-- Start comment form -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="comment-form-area">
                                    <div class="post-title mb-40">
                                        <h2 class="h6 inline-block">Sizin Mesajınız</h2>
                                    </div>
                                    <div class="form-wrap">
                                        <form action="#">
                                            <div class="form-inner clearfix">
                                                <div class="single-input left width-half">
                                                    <input required="required" placeholder="İsim Soyisim *" type="text">
                                                </div>
                                                <div class="single-input right width-half">
                                                    <input placeholder="Telefon Numarası" type="text">
                                                </div>
                                                <div class="single-input left width-half">
                                                    <input required="required" placeholder="Email *" type="text">
                                                </div>
                                                <div class="single-input right width-half">
                                                    <input required="required" placeholder="Website" type="text">
                                                </div>
                                                <div class="single-input">
                                                    <textarea class="textarea" placeholder="Mesajınızı Yazınız..."></textarea>
                                                </div>
                                                <button class="submit-button" type="submit">Mesajı Gönder</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End comment form -->
                        </div>
                    </div>
                    <!-- End left side -->
                    <!-- Start Right sidebar -->
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-warp columns">
                        <div class="row">
                            <aside class="zm-post-lay-a-area col-sm-6 col-md-12 col-lg-12 sm-mt-50 xs-mt-50">
                                <div class="row mb-40">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="section-title">
                                            <h2 class="h6 header-color inline-block uppercase">Trapez Kadro</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="zm-posts">
                                            <article class="zm-post-lay-a sidebar">
                                                <div class="zm-post-thumb">
                                                    <a href="#"><img src="/images/post/b/1.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title h2"><a href="#">Bu akşamın konukları Atilla OKTAY ve Ahmet TORPİL.</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">29 Ocak 2019 19:00</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <!-- Start Subscribe From -->
                            <div class="col-md-12 col-lg-12 col-sm-6 mt-60 sm-mb-50">
                                <aside class="subscribe-form bg-dark text-center sidebar">
                                    <h3 class="uppercase zm-post-title">Bisikletli Kadın E-Bülten</h3>
                                    <p>Türkiye'deki 6500'den fazla kadının üye olduğu topluluğumuzdan haberdar olmak için</p>
                                    <form action="#">
                                        <input type="text" placeholder="İsim Soyad">
                                        <input type="email" placeholder="Eposta Adresi" required="">
                                        <input type="submit" value="E-Bültene Kaydol">
                                    </form>
                                </aside>
                            </div>
                            <!-- End Subscribe From -->
                            <!-- Start post layout E -->
                            <aside class="zm-post-lay-e-area col-xs-12 col-sm-6 col-md-6 col-lg-12 mt-60 hidden-md">
                                <div class="row mb-40">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="section-title">
                                            <h2 class="h6 header-color inline-block uppercase">Kadın ve Kent</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="zm-posts">
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/kadin-ve-kent/1.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Kadın Dostu Kent Değil, Kent Dostu Kadınlar Var</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Çiçek TAHAOĞLU</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">26 Eylül 2014</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/kadin-ve-kent/2.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Bisiklet, Kadınların Toplumda Görünür Olması İçin Bir Fırsattır</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Pınar PİNZUTİ</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">26 Haziran 2017</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post hidden-md clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/kadin-ve-kent/3.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">'Süslü kadınlar', egzoz kokusu yerine parfüm kokusu için pe...</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Sema GÜR</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">23 Eylül 2018</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post hidden-md clearfix hidden-sm">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/kadin-ve-kent/4.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Toplumsal cinsiyetin kent ve mekanla ilişkisi ne?</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">KAOS GL</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">09 Mart 2017</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post hidden-md clearfix hidden-sm">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/kadin-ve-kent/5.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Alternatif Ulaşım Aracı Olarak Bisikleti Düşünmek</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Nevra TAŞLIDAN</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">06 Aralık 2010</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post hidden-md clearfix hidden-sm">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/kadin-ve-kent/6.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Ulaşımda ‘Pembe’ Ayrımcılığına Karşı Çıkmamız İçin 5 Neden</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Editör</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">19 Ekim 2017</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <!-- Start post layout E -->
                            <aside class="zm-post-lay-f-area col-sm-6 col-md-12 col-lg-12 mt-70">
                                <div class="row mb-40">
                                    <div class="col-md-12">
                                        <div class="section-title">
                                            <h2 class="h6 header-color inline-block uppercase">Etiketler</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="zm-tagcloud">
                                            <a href="#">Bisiklet Eğitimleri</a>
                                            <a href="#">Sürüş Etkinlikleri</a>
                                            <a href="#">Buluşmalar</a>
                                            <a href="#">Pedallayan Kadınlar</a>
                                            <a href="#">Bisikletli Yaşam</a>
                                            <a href="#">Etkinlik</a>
                                            <a href="#">Bisiklet Festivallari</a>
                                            <a href="#">Basın</a>
                                            <a href="#">Film ve Belgeseller</a>
                                            <a href="#">Nasıl Başlarım</a>
                                        </div>
                                    </div>
                                </div>
                            </aside>
                            <!-- Start post layout E -->
                            <aside class="zm-post-lay-f-area col-sm-6 col-md-12 col-lg-12 mt-70">
                                <div class="row mb-40">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="section-title">
                                            <h2 class="h6 header-color inline-block uppercase">Pedallayan Kadınlar</h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="zm-posts">
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/e/1.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Aygül ORHAN, üç yıldır işine bisikletle gidiyor.</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">25 Aralık 2018</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/e/2.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">İrem ÇAĞIL kızıyla birlikte bisikletle Kuzey Avrupa'yı dolaştı.</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">25 Mayıs 2017</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/e/3.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Şeyma ŞAHİN, bankadaki işini bırakıp, bisiklet dükkanı açtı.</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">14 Şubat 2017</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                            <!-- Start single post layout E -->
                                            <article class="zm-post-lay-e zm-single-post hidden-md clearfix">
                                                <div class="zm-post-thumb f-left">
                                                    <a href="#"><img src="/images/post/e/4.jpg" alt="img"></a>
                                                </div>
                                                <div class="zm-post-dis f-right">
                                                    <div class="zm-post-header">
                                                        <h2 class="zm-post-title"><a href="#">Aslıhan SEVEN, Polenezköy'deki nikah törenini bisikletiyle gitti.</a></h2>
                                                        <div class="zm-post-meta">
                                                            <ul>
                                                                <li class="s-meta"><a href="#" class="zm-author">Editor</a></li>
                                                                <li class="s-meta"><a href="#" class="zm-date">19 Eylül 2016</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- Start single post layout E -->
                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                    <!-- End Right sidebar -->
                </div>
            </div>
        </div>
    </div>
    <!-- End page content -->
@endsection